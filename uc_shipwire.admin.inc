<?php

/**
 * Settings form for Shipwire integration.
 */
function uc_shipwire_admin_settings() {
  $form = array();

  $form['uc_shipwire_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#description' => t('The email address corresponding to the Shipwire account.'),
    '#default_value' => variable_get('uc_shipwire_username', ''),
    '#required' => TRUE,
  );

  $form['uc_shipwire_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password for the shipwire account.'),
    '#default_value' => variable_get('uc_shipwire_password', ''),
    '#required' => TRUE,
  );
  
  $form['uc_shipwire_server'] = array(
    '#type' => 'select',
    '#title' => t('Server'),
    '#description' => t('This should be set to "Production" to use the live environment.'),
    '#options' => array('Test' => 'Test', 'Production' => 'Production'),
    '#default_value' => variable_get('uc_shipwire_server', 'Test'),
    '#required' => TRUE,
  );
  
  return system_settings_form($form);
}

function theme_uc_shipwire_option_label($quote) {
  $delivery = (array)$quote['DeliveryEstimate'];
  if ($delivery['Minimum'] == $delivery['Maximum']) {
    $range = $delivery['Minimum'];
  }
  else {
    $range = $delivery['Minimum'] .' - '. $delivery['Maximum'];
  }
    
  $output = t('@service (@range days)', array('@service' => $quote['Service'], '@range' => $range));

  return $output;
}


